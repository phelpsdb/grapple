from scripts import logger
import bge
from bge import logic
import mathutils as mu

LOOKAHEAD = 1
REAR_OFFSET = 0.5
SHOOTABLE_MASK = 1#65535
#PLAYER_COL_GROUP = 1 << 1
#SHOOTABLE_MASK ^= PLAYER_COL_GROUP

def castRay(cont):
    own = cont.owner
    ray = own.worldLinearVelocity / logic.getAverageFrameRate()
    # we start the raycast behind the bullet so that we avoid missing objects right in our face
    result = own.rayCast(own.worldPosition + ray * LOOKAHEAD + own.localOrientation * mu.Vector((0,0,-REAR_OFFSET)), None, 0, "", 0, 0, 0, SHOOTABLE_MASK)
    if result[1] is not None:
        own.worldPosition = result[1]
        cont.activate(cont.actuators['add_splosion'])
        cont.activate(cont.actuators['destroy'])

