import random
import math
import mathutils as mu
from math import pi
import bge
from bge import logic
from scripts import logger

OBJECT_SPEED = 0.07

def fillTunnel(cont):
    own = cont.owner
    start_pos = own.worldPosition.copy()
    adder = cont.actuators['add_object']
    interval = (own['min_spacing'] + own['max_spacing']) / 2.0 * OBJECT_SPEED
    curr = 0.0
    while (curr < own['tunnel_length']):
        own.worldPosition += getRandomTunnelVector(cont)
        own.worldPosition.y = start_pos.y + curr
        curr += interval
        adder.instantAddObject()
        own.worldPosition = start_pos

def generate(cont):
    own = cont.owner
    delay = cont.sensors['Delay']
    if not delay.positive:
        return
    delay.delay = random.randint(own['min_spacing'], own['max_spacing'])

    adder = cont.actuators['add_object']
    start_pos = own.worldPosition.copy()
    own.worldPosition += getRandomTunnelVector(cont)
    adder.instantAddObject()
    own.worldPosition = start_pos

def getRandomTunnelVector(cont):
    own = cont.owner
    randPosVector = mu.Vector((1, 0, 0))
    randPosVector.rotate(mu.Euler((0, 2 * math.pi * random.random(), 0)))
    return randPosVector * own['spread_radius'] * random.random()


