import bge
import mathutils as mu
from scripts import logger
from bge import logic
from scripts.game_utils import isStaticPhysicsType

class Kink:

    # position is a mathutils.Vector representing where this kink sits
    # parentObj is the object that this kink is attached to
    # ropeObj is a KX_GameObject representing the rope to use, or None
    # ropeCollisionCallback is the method to be called when
    # the rope collides with something
    def __init__(self, worldPosition, parentObj, ropeObj, ropeCollisionCallback, isFirst=False):
        self.position = worldPosition
        self.parent = parentObj
        self._localPosition = parentObj.worldTransform.inverted(mu.Matrix.Identity) * worldPosition
        self.isStatic = isStaticPhysicsType(parentObj)
        self.nextKink = None
        self.length = 0
        self.rope = logic.getCurrentScene().addObject(ropeObj, None, 0)
        self.isFirst = isFirst
        #self.rope.collisionCallbacks.append(ropeCollisionCallback)

    def updatePosition(self):
        if not self.parent.invalid:
            self.position = self.parent.worldTransform * self._localPosition

    def updateRope(self):
        if self.nextKink is not None:
            self.length = (self.nextKink.position - self.position).magnitude
            self.rope.worldPosition = self.position
            # ugly hack to prevent the rope from being 1 frame behind its attachment to the pod itself.
            visual_offset = mu.Vector((0,0,0))
            if self.isFirst:
                # Should be some sort of function, not parent.parent. This will certainly break.
                visual_offset = self.parent.parent.worldLinearVelocity / bge.logic.getLogicTicRate()
            self.rope.worldPosition += visual_offset
            self.rope.localScale.y = self.length
            if self.length > 0.01:
                self.rope.alignAxisToVect(self.nextKink.position - self.position - visual_offset, 1)

    def destroy(self):
        if self.rope is not None:
            self.rope.endObject()
