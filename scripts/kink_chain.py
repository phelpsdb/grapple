import mathutils as mu
from scripts.kink import Kink
from scripts.hit_result import HitResult
import scripts.logger as logger
from scripts.grapple_constants import TARGETABLE_LAYERS_MASK, VERTICAL_HELP, GID_REPEL, GID_ATTRACT, MAX_ROPE_LENGTH
import bge


# The margin is absolutely necessary;
# it prevents kink positions from being
# directly attached to the wall, thus
# missing the wall when doing raycast checks
KINK_MARGIN = 0.05

ATTACHED_OBJ_IMPULSE_COEFF = 0.02

#TARGETABLE_LAYERS_MASK = 253
class KinkChain:
    def __init__(self, owner, ropeObj, grappleType):
        self.owner = owner
        self.root = None
        self.tip = None
        self.length = 0
        self.rope = ropeObj
        self.grappleType = grappleType

    def appendKink(self, position, obj, rope):
        kink = Kink(position, obj, rope, self.addCollisionKink)
        if self.root is None:
            kink.isFirst = True
            self.root = kink
        else:
            self.tip.nextKink = kink
        kink.nextKink = None
        self.tip = kink
        self.length += 1

    def updateKinks(self):
        self.kinkLooper(lambda kink: kink.updatePosition())
        self.createAndRemoveKinks()
        self.applyImpulseToAttachedObjects()
        self.kinkLooper(lambda kink: kink.updateRope())

    """
    This method is ugly. Basically, it goes through the
    entire chain and adds or removes kinks as necessary
    """
    def createAndRemoveKinks(self):
        kink = self.root
        while kink is not None:
            nk = kink.nextKink
            if nk is not None:
                if nk.parent.invalid:
                    #remove a kink whose parent no longer exists
                    self.removeKink(nk, kink)
                    continue

                hitresult = self.getObstruction(kink, nk)
                if hitresult is not None:
                    # There should be a kink added, a new obstruction exists

                    # Move the obstruction point to the nearest edge of the polygon
                    #hitresult.recalculateToNearestEdge()

                    kinkpos = hitresult.hitpoint
                    # give it a slight margin off the normal (very important)
                    kinkpos += KINK_MARGIN * hitresult.hitnorm
                    # offset by the velocity of the object for one frame (important when attaching to moving objects)
                    kinkpos += hitresult.hitobj.worldLinearVelocity / bge.logic.getLogicTicRate()

                    """
                    Apparently this does more harm than good. Right now we are
                    not experiencing any problems with precision when dealing
                    with rotating anchors, but if we do, look into this further.

                    # offset by the angular velocity too, of the object for one frame (important when attaching to rotating objects)
                    objCenterToKink = kinkpos - hitresult.hitobj.worldPosition
                    objCenterToKink.rotate(mu.Euler(hitresult.hitobj.worldAngularVelocity / bge.logic.getLogicTicRate()))
                    kinkpos = objCenterToKink + hitresult.hitobj.worldPosition
                    """

                    newKink = Kink(kinkpos, hitresult.hitobj, self.rope, self.addCollisionKink)
                    kink.nextKink = newKink
                    newKink.nextKink = nk
                    self.length += 1
                    kink = nk
                    continue
                elif nk.nextKink is not None:
                    hitresult = self.getObstruction(kink, nk.nextKink)
                    # The middle kink should not exist if there's a clear path between it's adjacent kinks.
                    # This model is obviously imperfect
                    if hitresult is None:
                        self.removeKink(nk, kink)
            kink = kink.nextKink

    def removeKink(self, kink, prevK):
        prevK.nextKink = kink.nextKink
        kink.destroy()
        self.length -= 1

    def addCollisionKink(self, obj, point, normal):
        print(obj)

    def getObstruction(self, kink1, kink2):
        rayLength = (kink2.position - kink1.position).magnitude - 0.01
        result = self.owner.rayCast( kink2.position, kink1.position,
                (rayLength if rayLength > 0 else 0),
                "", 1, 0, 1, TARGETABLE_LAYERS_MASK)
        if result[0] is not None:
            return HitResult(*result)
        else:
            return None

    def applyImpulseToAttachedObjects(self):
        if self.root is None:
            return
        kink = self.root
        while kink.nextKink is not None:
            nk = kink.nextKink
            if not nk.isStatic:
                force = kink.position - nk.position
                #force.z *= VERTICAL_HELP
                if self.grappleType is GID_REPEL:
                    force *= -(MAX_ROPE_LENGTH - force.length) / MAX_ROPE_LENGTH
                force *= ATTACHED_OBJ_IMPULSE_COEFF
                nk.parent.applyImpulse(nk.position, force, False)
            kink = nk
            if self.grappleType is GID_REPEL: # only run once for the repeller
                return

    def kinkLooper(self, kinkFunction):
        kink = self.root
        while kink is not None:
            kinkFunction(kink)
            kink = kink.nextKink

    """
    Iterate all kinks over a function to return a single compounded value
    """
    def kinkFolder(self, kinkFoldFunction, initialValue):
        kink = self.root
        while kink is not None:
            initialValue = kinkFoldFunction(kink, initialValue)
            kink = kink.nextKink
        return initialValue


    def clearKinks(self):
        nextKink = self.root
        while nextKink is not None:
            nextKink.destroy()
            nextKink = nextKink.nextKink
        self.root = self.tip = None
        self.length = 0

    """
    Return the vector from the first kink to the second.
    Returned vector is of zero length if chain is empty or length 1
    """
    def getMovementVector(self):
        if self.length > 1:
            return self.root.nextKink.position - self.root.position
        else:
            return mu.Vector((0,0,0))

    def getTotalRopeLength(self):
        return self.kinkFolder(lambda kink, val: val + kink.length, 0)



