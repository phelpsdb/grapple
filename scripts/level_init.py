from bge import logic

def main():
  
    # when everything is done, we broadcast the message.
    cont = logic.getCurrentController()
    cont.activate(cont.actuators["level_init"])
  
main()