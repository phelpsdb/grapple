from scripts import logger
import bge
from bge import logic

def initGun(cont):
    logger.debug("gun initializing")
    own = cont.owner
    try:
        if own['_init'] == True:
            return
    except:
        pass
    own['_init'] = True

def shoot(cont):
    if cont.sensors['shoot'].positive:
        cont.actuators['gun_sound'].stopSound()
        cont.actuators['gun_sound'].startSound()
        cont.activate(cont.actuators['add_bullet'])
