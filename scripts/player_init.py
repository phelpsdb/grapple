import bge
from bge import logic
import mathutils as mu
from scripts.game_utils import hasProperty

SHIP_SPIN = 0.001

def initPlayer(cont):
    scene = logic.getCurrentScene()
    player = cont.owner
    player['cam_pivot'] = scene.objects['camera_target']
    player['camera'] = scene.objects['player_cam']
    player['camera']['relative_offset'] = player['camera'].localPosition.copy()
    player['character'] = scene.objects['character_parent']
    # Parenting the group instance to the hover doesn't work in the editor.
    player['character'].setParent(player)
    player['ship'] = scene.objects['player_ship']
    player['lean_ctrl'] = scene.objects['lean_ctrl']
    player['lean_ctrl']['original_pos'] = player['lean_ctrl'].localPosition.copy()
    player['previous_vel'] = mu.Vector((0,0,0))
    #player['character'].meshes[0].materials[0].alpha = 0.5
    for act in cont.actuators:
        cont.activate(act)

def spawnPlayer(cont):
    scene = logic.getCurrentScene()
    try:
        start_obj = scene.objects['player_start']
    except:
        start_obj = scene.objects['player_start.001']
    reset_pos = start_obj.worldPosition
    cont.owner.worldPosition = reset_pos
    cont.owner.worldLinearVelocity = [0,0,0]
    cont.owner.worldOrientation = start_obj.worldOrientation

def zeroVelocity(cont):
    cont.owner.worldLinearVelocity = (0,0,0)

def wallBounce(cont):
    own = cont.owner
    hitObj = cont.sensors['bounce'].hitObject
    if hitObj is not None and not hasProperty(hitObj, 'is_bullet'):
        cont.activate(cont.actuators['bounce_sound'])

def shipSpin(cont):
    own = cont.owner
    spin = cont.actuators['spin']
    spin.dRot = mu.Vector((0,0,own.worldLinearVelocity.length_squared * SHIP_SPIN))
    cont.activate(spin)
