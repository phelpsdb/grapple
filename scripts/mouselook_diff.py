###############################################
#      ****** ORIGINAL HEADER ******
#  Customizable FPS Script (v2.4)
#   by Riyuzakisan (11/11/2011)
#   Made in Blender 2.60.0, r41226
#
#  Contact:
#   riyuzakisan@gmail.com
#
#  Released under the Creative Commons
#  Attribution 3.0 Unported License.
#
###############################################
#      ****** MODIFIED HEADER ******
#   This script was modified by Daniel Phelps
#   to suit personalized project needs.
#   Contact: phelps.db@gmail.com
###############################################

from bge import logic
from bge import render
from bge import events as e
from math import pi
from math import radians
#import control_config as cc
import mathutils as mu
import math
import logger

MOUSELOOK_CAP = 140 # Total boundary of vertical mouselook from 0 (no freedom) to 180 (full freedom)
MOUSE_SENSITIVITY = 0.0030
MOUSE_INVERT = 1

CAM_THIRD_PERSON = 0
CAM_FIRST_PERSON = 1
CAM_FOLLOW_MAX_DIST = 1.8

# the initial distance from the camera to its pivot.
CAM_DEFAULT_DIST_TO_PIVOT = 2


"""
(Internal Use) Calculates the amount of horizontal and vertical mouse
movement as it affects the character.
Return: A tuple (x, y) of horizontal and vertical movement.
"""
def mouseMove(cont):
    mouse = cont.sensors['mouse_move']
    own = cont.owner
    if mouse.positive == False:
        x = 0
        y = 0
        cont.owner['mousemove'] = (x, y)
        return (x, y)
    sizex = render.getWindowWidth()
    sizey = render.getWindowHeight()
    x = math.floor(sizex/2) - mouse.position[0]
    y = math.floor(sizey/2) - mouse.position[1]
    #logger.debug("mousemove", winx=sizex/2, winy=sizey/2, mx=mouse.position[0], my=mouse.position[1], final_x=x, final_y=y)
    if '_mouse_init' not in own:
        own['_mouse_init'] = True
        x = 0
        y = 0
    horz = x * MOUSE_SENSITIVITY * MOUSE_INVERT
    vert = y * MOUSE_SENSITIVITY * MOUSE_INVERT
    if isMouseCapped(cont, vert) == True:
        vert = 0
    cont.owner['mousemove'] = (horz, vert)
    # Prevents the mouse_move sensor from being triggered unnecessarily
    # by render.setMousePosition.
    if mouse.position[0] != int(sizex/2) or mouse.position[1] != int(sizey/2):
        # center the cursor
        render.setMousePosition(int(sizex/2), int(sizey/2))
    return (horz, vert)

"""
(Internal Use) Determines whether the given vertical mouse movement
is out of the limited view angle.
"""
def isMouseCapped(cont, vert_move):
    # note: the camera pivot is not oriented the same as the
    # camera, meaning Y axis looks forward instead of -Z
    own = cont.owner
    pivori = own['cam_pivot'].localOrientation.to_euler()
    offset = math.pi / 2.0
    final_angle = pivori.x + vert_move
    #logger.debug("Angles", cam=pivori.x, move=vert_move, final=final_angle)
    # change range from (deg) [-90, 90] to [0, 180] (easier math)
    final_angle += offset
    clip_area = radians((180 - MOUSELOOK_CAP) / 2.0)
    if final_angle < clip_area or final_angle > (radians(MOUSELOOK_CAP) + clip_area):
        #logger.debug("Capped!", final=final_angle, clip=clip_area)
        return True
    else:
        return False

def jump(cont):
    if own['ground'] == True:
        own.localLinearVelocity[2] = DEFAULT_JUMP_FACTOR * DEFAULT_JUMP_OFFSET

"""
Updates on mouse movement when the player is in the First Person state.
"""
def setCameraFirstPerson(cont):
    own = cont.owner
    cam_pivot = own['cam_pivot']
    horz, vert = mouseMove(cont)
    pivori = cam_pivot.localOrientation.to_euler()
    # The cam always has its own vertical orientation.
    #pivori.x += vert
    # The camera's horizontal orientation will copy the player's.
    ori2 = own.localOrientation.to_euler()
    ori2.z += horz
    pivori.z = 0
    cam_pivot.localOrientation = pivori
    own.localOrientation = ori2
    # The camera pivot copies the orientation of the player.
    # if I knew how to work with transform matrices, this would be simpler
    #pivori.z = own.worldOrientation.to_euler().z
    #cam_pivot.worldOrientation = pivori
    #cam_pivot.localPosition.x = 0.2 # over the shoulder
    
    # Move the camera closer to the player if it hits a wall
    dist = checkDistToObstruction(cont)
    scale = dist / CAM_DEFAULT_DIST_TO_PIVOT
    #logger.debug("distance, scale", dist, scale)
    cam_pivot.localScale = (scale, scale, scale)

"""
Updates on mouse movement when the player is in the Third Person state.
"""
def setCameraThirdPerson(cont):
    own = cont.owner
    cam_pivot = own['cam_pivot']
    horz, vert = mouseMove(cont)
    pivori = cam_pivot.localOrientation.to_euler()
    scaleCameraToDistance(cam_pivot, checkDistToObstruction(cont))
    # Reset the pivot's original position.
    cam_pivot.localPosition.x = 0
    # The cam always has its own vertical orientation.
    #pivori.x += vert
    # The player's horizontal orientation is independent of the camera's.
    pivori.z += horz
    cam_pivot.localOrientation = pivori

"""
(Internal Use) Checks if anything is between the player and the camera
in 3rd person mode. Returns the distance to the obstruction or the default
distance if none.
"""
def checkDistToObstruction(cont):
    cam = cont.owner['camera']
    cam_pivot = cont.owner['cam_pivot']
    hit = cam_pivot.rayCast(cam, cam_pivot, CAM_DEFAULT_DIST_TO_PIVOT)[1]
    if hit == None:
        return CAM_DEFAULT_DIST_TO_PIVOT
    else:
        #logger.debug("distance to hit", cam_pivot.getDistanceTo(hit))
        return cam_pivot.getDistanceTo(hit)
