# A bitmask representing what object layers the crosshair can target.
# exclude layers 1 and 3
TARGETABLE_LAYERS_MASK = 255 - 2 - 4

GRAPPLE_RANGE = 23
MAX_ROPE_LENGTH = 46

# Multiplier for the z component of the grapple's force vector
VERTICAL_HELP = 3.0

GID_ATTRACT = 0
GID_REPEL = 1

