from scripts import logger
"""
This class exists so that the BGE's implementation
of KX_GameObject.rayCast() is decoupled from our
own logic. Maybe this is overengineering, but whatevs.
"""
class HitResult:

    def __init__(self, hitobj, hitpoint, hitnorm, hitpoly):
        self.hitobj = hitobj
        self.hitpoint = hitpoint
        self.hitnorm = hitnorm
        self.hitpoly = hitpoly

    def recalculateToNearestEdge(self):
        if self.hitpoly is None:
            return

        logger.debug("doin it")
        mesh = self.hitpoly.getMesh()
        mid = self.hitpoly.material_id
        verts = []
        verts.append(mesh.getVertex(mid, self.hitpoly.v1))
        verts.append(mesh.getVertex(mid, self.hitpoly.v2))
        verts.append(mesh.getVertex(mid, self.hitpoly.v3))
        if self.hitpoly.v4 != 0:
            verts.append(mesh.getVertex(mid, self.hitpoly.v4))

        v1 = verts[len(verts) - 1]
        shortest_dist = float("inf")
        shortest_dist_edge = (None, None)
        shortest_edge_p = None
        hit = self.hitobj.worldTransform.inverted_safe() * self.hitpoint
        i = 0
        while i < len(verts):
            v2 = verts[i]
            ab = v2.XYZ - v1.XYZ
            ap = hit - v1.XYZ
            edge_p = ap.project(ab)
            edge_p += v1.XYZ
            p_to_edge = edge_p - hit
            lensq = p_to_edge.length_squared
            if lensq < shortest_dist:
                shortest_dist = lensq
                shortest_dist_edge = (v1, v2)
                shortest_edge_p = edge_p

            v1 = v2
            i+=1
        logger.debug("normal", (shortest_dist_edge[0].normal + shortest_dist_edge[1].normal))
        self.hitpoint = self.hitobj.worldTransform * shortest_edge_p
        self.hitnorm = -(self.hitobj.worldTransform * (shortest_dist_edge[0].normal + shortest_dist_edge[1].normal)).normalized()
        logger.debug("final normal", self.hitnorm)


