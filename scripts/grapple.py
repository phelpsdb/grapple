import bge
import scripts.logger as logger
import mathutils as mu
from bge import logic
from scripts.kink import Kink
from scripts.kink_chain import KinkChain
from scripts.kink_chain import KINK_MARGIN
from scripts.grapple_constants import TARGETABLE_LAYERS_MASK, GRAPPLE_RANGE, MAX_ROPE_LENGTH, VERTICAL_HELP, GID_ATTRACT, GID_REPEL

FORCE_COEF = 1.2
HOOK_SIZE = 0.15

# Multiplier for the repel type grapple component of the grapple's force vector
REPEL_COEF = 5

def initGrapple(cont):
    logger.debug("grapple initializing")
    own = cont.owner
    try:
        if own['_init'] == True:
            return
    except:
        pass

    g = own['grapple_id']
    own['_init'] = True
    own['hook_pos'] = mu.Vector([0,0,0])
    own['aim_pos'] = mu.Vector([0,0,0])
    own['aux_rope'] = "grapple_aux_rope_" + str(g)
    own['kink_chain'] = KinkChain(own, own['aux_rope'], own['grapple_id'])
    logger.debug("grapple id: ", g)
    scene = logic.getCurrentScene()
    own['crosshair'] = scene.objects["crosshair"]
    own['hook'] = scene.objects["grapple_hook_" + str(g)]
    own['rope'] = scene.objects["grapple_rope_" + str(g)]
    cont.activate(cont.actuators['to_idle_state'])


################## Idle State #######################

def initIdle(cont):
    own = cont.owner
    own['hook_pos'] = mu.Vector([0,0,0])
    own['rope'].localScale.y = 0
    own['kink_chain'].clearKinks()

def updateIdle(cont):
    own = cont.owner
    rope_act = cont.actuators['rope_extend']

    #logger.debug("hooked at", own['hook_pos'])
    #logger.debug("World Orientation", -own.worldOrientation[1])
    #logger.debug("Hit position was", hitpos)
    #logger.debug("crosshair position", crosshair.worldPosition)

    own['hook'].worldPosition = own.worldPosition
    # position the hook in front of the grapple gun
    own['hook'].worldPosition -= own.getAxisVect(mu.Vector([0,0,HOOK_SIZE]))
    # offset hook position by our current speed.
    own['hook'].worldPosition += own.parent.worldLinearVelocity / bge.logic.getLogicTicRate()

    own['rope'].localScale.y = 0

def shoot(cont):
    own = cont.owner
    if cont.sensors['mouseclick'].positive:
        # shoot grapple
        cont.activate(cont.actuators['grapple_fire'])
        #logger.debug("Shot and aim_pos was", own['crosshair']['aim_pos'])
        cont.activate(cont.actuators['to_shooting_state'])
    else:
        cont.deactivate(cont.actuators['grapple_fire'])


################## Shooting State #######################

def initShooting(cont):
    own = cont.owner
    own['rope'].localScale.y = 0

def updateShooting(cont):
    own = cont.owner

    # extend the grapple
    own['rope'].localScale.y = min(own['rope'].localScale.y + own['hook_speed'], GRAPPLE_RANGE)
    #own['rope'].localScale.y += own['hook_speed']
    own['rope'].alignAxisToVect(own['crosshair'].worldPosition - own.worldPosition, 1)
    # position the hook at the end of the rope
    own['hook'].worldPosition = (own['crosshair'].worldPosition - own.worldPosition) \
            * own['rope'].localScale.y + own.worldPosition

    if not cont.sensors['mouseclick'].positive:
        cont.activate(cont.actuators['to_idle_state'])
    elif didHitObjectWithHook(own):
        #logger.debug("Hit, and aim_pos was", own['crosshair']['aim_pos'])
        cont.activate(cont.actuators['grapple_hooked_sound'])
        own['hook_pos'] = own['crosshair']['aim_pos'].copy() + own['crosshair']['aim_pos_norm'] * KINK_MARGIN
        createInitialRopeKinks(own)

        # create a decorative star that identifies where the hook hit
        star = logic.getCurrentScene().addObject(
                cont.actuators['add_hit_star'].object,
                own['hook'],
                cont.actuators['add_hit_star'].time)
        star.worldPosition = own['hook_pos'] + own['crosshair']['aim_pos_norm'] * 0.02
        star.alignAxisToVect(own['crosshair']['aim_pos_norm'], 1)

        cont.activate(cont.actuators['to_hooked_state'])

def createInitialRopeKinks(own):
    own['kink_chain'].clearKinks()
    own['kink_chain'].appendKink(own.worldPosition.copy(), own, own['aux_rope'])
    own['kink_chain'].appendKink(own['hook_pos'].copy(), own['crosshair']['aim_obj'], own['aux_rope'])

def didHitObjectWithHook(own):
    return own['rope'].localScale.y >= \
            (own['crosshair']['aim_pos'] - own.worldPosition).magnitude and \
            own['crosshair']['aimed']


################## Hooked State #######################

def initHooked(cont):
    cont.owner['rope'].localScale.y = 0

def updateHooked(cont):
    own = cont.owner
    chain_size = own['kink_chain'].length
    own['chain_size'] = chain_size
    if (chain_size < 2):
        cont.activate(cont.actuators['to_idle_state'])

    if own['grapple_id'] is GID_ATTRACT:
        _updateHookedForGrappleType(cont, _getForceVecAttract, _shouldUnhookAttract)
    elif own['grapple_id'] is GID_REPEL:
        _updateHookedForGrappleType(cont, _getForceVecRepel, _shouldUnhookRepel)

def _updateHookedForGrappleType(cont, getForceVecFunc, shouldUnhookFunc):
    own = cont.owner
    own['kink_chain'].updateKinks()
    rope_length = own['kink_chain'].getTotalRopeLength()
    if shouldUnhookFunc(cont, rope_length):
        cont.activate(cont.actuators['to_idle_state'])
        return
    moveVec = own['kink_chain'].getMovementVector()
    forceVec = getForceVecFunc(own, moveVec, rope_length)
    forceVec.z *= VERTICAL_HELP
    #logger.debug("force vec and magn", force_vec, force_vec.magnitude)
    own.parent.applyForce(FORCE_COEF * forceVec)
    own['hook'].worldPosition = own['kink_chain'].tip.position

def _shouldUnhookAttract(cont, totalRopeLength):
    return _snapRopeIfLongerThan(cont, totalRopeLength, MAX_ROPE_LENGTH) or not cont.sensors['mouseclick'].positive

def _shouldUnhookRepel(cont, totalRopeLength):
    return _snapRopeIfLongerThan(cont, totalRopeLength, GRAPPLE_RANGE) or not cont.sensors['mouseclick'].positive

def _snapRopeIfLongerThan(cont, rope_length, max_length):
    if rope_length > max_length:
        cont.activate(cont.actuators['snap_sound'])
        return True
    return False

def _getForceVecAttract(own, movementVector, totalRopeLength):
    return movementVector.normalized() * totalRopeLength

def _getForceVecRepel(own, movementVector, totalRopeLength):
    return (-movementVector).normalized() * \
            ((GRAPPLE_RANGE - totalRopeLength) / GRAPPLE_RANGE) * \
            REPEL_COEF
