
def debug(message, *args, **kwargs):
    debug_string = "[DEBUG]  " + message
    if len(debug_string) < 40:
        for i in range(40 - len(debug_string)): debug_string += " "
    debug_string += ": "
    for arg in args:
        debug_string += str(arg) + ", "
    debug_string = debug_string.rstrip(", ") + "  "
    for key in kwargs:
        debug_string += key + ": " + str(kwargs[key]) + ", "
    debug_string = debug_string.rstrip(":, ")
    print(debug_string)

def error(message, exception = None):
    error_string = "[ERROR]  " + message
    if exception != None:
        error_string += ": " + exception
    print(error_string)

def info(message):
    print("[INFO]  " + message)
