from bge import logic
from bge import render
from bge import events as e
from math import pi
from math import radians
import mathutils as mu
import math
from scripts.game_utils import lerpVector
from scripts import logger

MOUSELOOK_CAP = 170 # Total boundary of vertical mouselook from 0 (no freedom) to 180 (full freedom)
MOUSE_SENSITIVITY = 0.0012
MOUSE_INVERT = 1

CAM_COLLISION_MASK = 1

CAM_THIRD_PERSON = 0
CAM_FIRST_PERSON = 1
CAM_FOLLOW_MAX_DIST = 1

# the initial distance from the camera to its pivot.
CAM_DEFAULT_DIST_TO_PIVOT = 1
DISTANCE_MARGIN = 0.05

CAM_DRAG_AMOUNT = 0.2
HEAD_DRAG_AMOUNT = 0.02
LEAN_Z_COEFF = 3


"""
(Internal Use) Calculates the amount of horizontal and vertical mouse
movement as it affects the character.
Return: A tuple (x, y) of horizontal and vertical movement.
"""
def mouseMove(cont):
    own = cont.owner

    # using own, not cont, to get the sensor because we don't want the
    # sensor to trigger any controllers.
    mouse = cont.sensors['mouse_move']

    if mouse.positive == False:
        x = 0
        y = 0
        cont.owner['mousemove'] = (x, y)
        return (x, y)
    sizex = render.getWindowWidth()
    sizey = render.getWindowHeight()
    x = math.floor(sizex/2) - mouse.position[0]
    y = math.floor(sizey/2) - mouse.position[1]
    #logger.debug("mousemove", winx=sizex/2, winy=sizey/2, mx=mouse.position[0], my=mouse.position[1], final_x=x, final_y=y)
    if '_mouse_init' not in own:
        own['_mouse_init'] = True
        x = 0
        y = 0
    horz = x * MOUSE_SENSITIVITY
    vert = y * MOUSE_SENSITIVITY * MOUSE_INVERT
    if isMouseCapped(cont, vert) == True:
        vert = 0
    cont.owner['mousemove'] = (horz, vert)
    # Prevents the mouse_move sensor from being triggered unnecessarily
    # by render.setMousePosition.
    if mouse.position[0] != int(sizex/2) or mouse.position[1] != int(sizey/2):
        # center the cursor
        render.setMousePosition(int(sizex/2), int(sizey/2))
    return (horz, vert)

"""
(Internal Use) Determines whether the given vertical mouse movement
is out of the limited view angle.
"""
def isMouseCapped(cont, vert_move):
    # note: the camera pivot is not oriented the same as the
    # camera, meaning Y axis looks forward instead of -Z
    own = cont.owner
    pivori = own['cam_pivot'].localOrientation.to_euler()
    offset = math.pi / 2.0
    final_angle = pivori.x + vert_move
    #logger.debug("Angles", cam=pivori.x, move=vert_move, final=final_angle)
    # change range from (deg) [-90, 90] to [0, 180] (easier math)
    final_angle += offset
    clip_area = radians((180 - MOUSELOOK_CAP) / 2.0)
    if final_angle < clip_area or final_angle > (radians(MOUSELOOK_CAP) + clip_area):
        #logger.debug("Capped!", final=final_angle, clip=clip_area)
        return True
    else:
        return False

def jump(cont):
    if own['ground'] == True:
        own.localLinearVelocity[2] = DEFAULT_JUMP_FACTOR * DEFAULT_JUMP_OFFSET

"""
Updates on mouse movement
"""
def setCameraFirstPerson(cont):
    own = cont.owner
    cam_pivot = own['cam_pivot']
    horz, vert = mouseMove(cont)
    pivori = cam_pivot.localOrientation.to_euler()
    # The cam always has its own vertical orientation.
    pivori.x += vert
    # The camera's horizontal orientation will copy the player's.
    ori2 = own.localOrientation.to_euler()
    ori2.z += horz
    pivori.z = 0
    cam_pivot.localOrientation = pivori
    own.localOrientation = ori2
    # The camera pivot copies the orientation of the player.
    # if I knew how to work with transform matrices, this would be simpler
    #pivori.z = own.worldOrientation.to_euler().z
    #cam_pivot.worldOrientation = pivori
    #cam_pivot.localPosition.x = 0.2 # over the shoulder

    # Move the camera closer to the player if it hits a wall
    dist = checkDistToObstruction(cont) - DISTANCE_MARGIN
    scale = dist / CAM_DEFAULT_DIST_TO_PIVOT
    #logger.debug("distance, scale", dist, scale)
    cam_pivot.localScale = (scale, scale, scale)
    reorient = mu.Matrix.Identity(3)
    reorient[0][0] = -1
    reorient[1][1] = -1
    own['character'].localOrientation = cam_pivot.localOrientation * reorient

def updateCameraDrag(cont):
    own = cont.owner
    accel = (own.localLinearVelocity - own['previous_vel'])
    target_lean = own.localLinearVelocity * HEAD_DRAG_AMOUNT
    own['previous_vel'] = own.localLinearVelocity.copy()
    """
    own['camera'].localPosition = lerpVector(
            own['camera'].localPosition,
            own['camera']['relative_offset'] - (accel * CAM_DRAG_AMOUNT),
            0.1
    )
    """
    lean = accel * HEAD_DRAG_AMOUNT
    target_lean.z = -target_lean.z * LEAN_Z_COEFF # increase the effect of up-down movement
    own['lean_ctrl'].localPosition = own['lean_ctrl'].localPosition.lerp(
            own['lean_ctrl']['original_pos'] + target_lean, 0.2)


"""
Checks if anything is between the player and the camera.
Returns the distance to the obstruction or the default distance if none.
"""
def checkDistToObstruction(cont):
    cam = cont.owner['camera']
    cam_pivot = cont.owner['cam_pivot']
    hit = cam_pivot.rayCast(cam, cam_pivot, CAM_DEFAULT_DIST_TO_PIVOT, "", 0, 0, 0, CAM_COLLISION_MASK)[1]
    if hit == None:
        return CAM_DEFAULT_DIST_TO_PIVOT
    else:
        #logger.debug("distance to hit", cam_pivot.getDistanceTo(hit))
        return cam_pivot.getDistanceTo(hit)
