import bge
import mathutils as mu

PARTICLE_GROWTH_SPEED = 1.1

def updateStar(cont):
    own = cont.owner
    own.localScale *= PARTICLE_GROWTH_SPEED
    own.color[3] /= PARTICLE_GROWTH_SPEED
