import bge
import scripts.logger as logger
import mathutils as mu
from bge import logic
from scripts.grapple import GRAPPLE_RANGE
from scripts.grapple_constants import TARGETABLE_LAYERS_MASK
from scripts.game_utils import hasProperty

def initCrosshair(cont):
    #logger.debug("crosshair initializing")
    own = cont.owner
    try:
        if own['_init'] == True:
            return
    except:
        pass
    own['_init'] = True
    own['aimed'] = False
    own['aim_pos'] = mu.Vector([0,0,0])
    own['aim_pos_norm'] = mu.Vector([0,0,0])
    own['aim_obj'] = None
    scene = logic.getCurrentScene()
    own['aimer'] = scene.objects["crosshair_aimer"]


def positionCrosshair(cont):
    own = cont.owner
    hitresult = own.rayCast(
        own,
        own['aimer'],
        GRAPPLE_RANGE,
        "", 0, 0, 0,
        TARGETABLE_LAYERS_MASK) #disabled for now because it only works latest builds
    #logger.debug("hooked at", own['hook_pos'])
    #logger.debug("World Orientation", -own.worldOrientation[1])
    #logger.debug("Hit position was", hitresult[1])
    #logger.debug("target position", target.worldPosition)
    own['aimed'] = False
    if hitresult[0] is not None:
        own['aim_pos'] = hitresult[1]
        own['aim_pos_norm'] = hitresult[2]
        own['aim_obj'] = hitresult[0]
        if not hasProperty(hitresult[0], 'nograpple'):
            own['aimed'] = True
            #logger.debug("hit position", hitresult[1])
            #logger.debug("aim pos", own['aim_pos'])
            cont.deactivate(cont.actuators['set_red_target'])
            cont.activate(cont.actuators['set_green_target'])
        else:
            cont.activate(cont.actuators['set_red_target'])
    else:
        # Multiply the orientation matrix by
        # the local vector (0,0,GRAPPLE_RANGE) to get
        # a position GRAPPLE_RANGE units out in front of the player,
        # where the crosshair will just hang out until we need it.
        own['aim_pos'] = own.worldOrientation * mu.Vector([0, 0, GRAPPLE_RANGE]) + own['aimer'].worldPosition
        #logger.debug("no hit position, target aimed at", own['aim_pos'])
        cont.activate(cont.actuators['set_red_target'])
    own.worldPosition = own['aim_pos']
