from bge import logic

def hasProperty(game_obj, property):
    has_prop = True
    try:
        game_obj[property]
    except:
        has_prop = False
        pass
    return has_prop

def isStaticPhysicsType(obj):
    return obj.visible and obj.getPhysicsId() and not obj.mass

def lerpVector(start, end, amount):
    return (start * (1.0 - amount)) + (end * amount)

def antigravity(cont):
    cont.owner.applyForce(-logic.getCurrentScene().gravity);

