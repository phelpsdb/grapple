import bge
from bge import logic
from scripts import logger

def setCheckpoint(cont):
    logger.debug("called")
    own = cont.owner
    if hasattr(own, 'activated') and own['activated']:
        return
    own['activated'] = True
    start = logic.getCurrentScene().objects['player_start']
    if start is not None:
        start.worldPosition = own.worldPosition
        start.worldOrientation = own.worldOrientation
