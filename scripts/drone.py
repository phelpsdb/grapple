import bge
from bge import logic
from scripts import logger

SPLOSION_DAMAGE_COEFF = 0.3
SPEED = 4

def droneInit(cont):
    own = cont.owner
    scene = logic.getCurrentScene()
    own['handleSplosion'] = handleSplosion
    own['time_since_player_seen'] = own['chase_blind_time']
    own['player_found'] = False
    """
    if scene.name == 'drone': # test scene
        steer = own.actuators['find_player']
        steer.target = scene.objects['test_hovercraft']
        steer.navmesh = scene.objects['test_navmesh']
    """

def getPlayer(cont):
    scene = logic.getCurrentScene()
    own = cont.owner
    steer = own.actuators['find_player']
    steer.target = scene.objects['hovercraft']
    own['time_since_player_seen'] = own['chase_blind_time']
    own['player_found'] = False

def update(cont):
    own = cont.owner
    if '_player' not in own:
        scene = logic.getCurrentScene()
        play = scene.objects['hovercraft']
        if play is not None:
            own['_player'] = play
        else:
            return

    steer = own.actuators['chase']

    own['player_found'] = False
    ray = own.rayCast(own['_player'], own)
    if ray[0] is own['_player']:
        own['player_found'] = True
        own['time_since_player_seen'] = 0
        steer.linV = (ray[1] - own.worldPosition).normalized() * SPEED;
    else:
        own['time_since_player_seen'] = min(own['chase_blind_time'], own['time_since_player_seen'] + 1)

    if own['player_found'] or own['time_since_player_seen'] < own['chase_blind_time']:
        #continue the chase for a little bit even if we lost the player
        cont.activate(steer)
    else:
        cont.deactivate(steer)

def handleSplosion(cont, magnitude):
    own = cont.owner
    own['health'] -= SPLOSION_DAMAGE_COEFF * magnitude

def reportDeath(cont):
    logger.debug('drone died')
