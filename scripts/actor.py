import bge
from scripts import logger

SPLOSION_MULT = 20

def handleSplosion(cont):
    own = cont.owner
    sensor = cont.sensors[0]
    for hit in sensor.hitObjectList:
        res = own.getVectTo(hit)
        radius = 1
        if 'radius' in hit:
            raduis = hit['radius']
        magn = max(0, radius - res[0])
        if magn == 0:
            return
        own.applyForce(-res[1] * SPLOSION_MULT * magn)
        if 'handleSplosion' in own:
            own['handleSplosion'](cont, magn)
